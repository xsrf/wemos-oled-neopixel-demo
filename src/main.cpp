#define FASTLED_ALLOW_INTERRUPTS 0
#include <Arduino.h>
#include "FastLED.h"
#include "Ticker.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// SDA: D2; SCL: D1
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

#define NUM_LEDS 10
#define DATA_PIN D4

CRGB leds[NUM_LEDS];

Ticker tick;
volatile uint8_t ledHue = 0;

void setup() {
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  display.display(); // Displays Adafruit Logo
  delay(1e3); // Pause
  display.clearDisplay(); // Clear the buffer

  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(10, 0);
  display.println(F("Hue: "));
  display.display(); // Show initial text

  tick.attach_ms(20,[]{
    ledHue++;
    for(uint8_t i=0; i<NUM_LEDS; i++) leds[i].setHue(ledHue + (255*i)/NUM_LEDS);
    FastLED.show();
    display.fillRect(10,24,120,48,BLACK); // Clear part of display
    display.setCursor(10, 24);
    display.println(ledHue);
    display.drawRect(10,48,104,8,WHITE);
    display.fillRect(12,50,100*ledHue/255,4,WHITE);
    //display.display(); // Write display buffer; Won't work within Ticker callback :(
  });
}

void loop() {
  display.display(); // Write display buffer
  delay(20);
}